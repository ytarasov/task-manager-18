package ru.t1.ytarasov.tm.repository;

import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User create(final String login, final String password) {
        User user = new User(login, HashUtil.salt(password));
        user.setRole(Role.USUAL);
        add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        User user = create(login, password);
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public void add(final User user) {
        users.add(user);
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User remove(User user) {
        users.remove(user);
        return user;
    }

    @Override
    public Boolean isLoginExists(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExists(final String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

}
