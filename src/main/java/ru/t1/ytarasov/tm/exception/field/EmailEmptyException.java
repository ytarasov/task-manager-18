package ru.t1.ytarasov.tm.exception.field;

import ru.t1.ytarasov.tm.exception.user.AbstractUserException;

public final class EmailEmptyException extends AbstractUserException {

    public EmailEmptyException() {
        super("Error! E-mail is empty...");
    }
}
