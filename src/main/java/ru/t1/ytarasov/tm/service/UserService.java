package ru.t1.ytarasov.tm.service;

import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.IUserService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.exception.user.*;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return userRepository.create(login, password);
    }

    @Override
    public User create(final String login, final String password, final String email) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        return userRepository.create(login, password, email);
    }

    @Override
    public User create(final String login, final String password, final Role role) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        return userRepository.create(login, password, role);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User removeById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeByEmail(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) throws AbstractException {
        if (id == null || id.isEmpty()) throw new  IdEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.isLoginExists(login);
    }

    @Override
    public Boolean isEmailExists(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.isEmailExists(email);
    }

}
