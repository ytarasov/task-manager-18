package ru.t1.ytarasov.tm.command.project;

import ru.t1.ytarasov.tm.exception.AbstractException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = "project-clear";

    public static final String DESCRIPTION = "Clear all projects";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR PROJECT LIST");
        getProjectTaskService().clearAllProjects();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
