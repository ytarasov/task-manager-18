package ru.t1.ytarasov.tm.command.user;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String NAME = "view-profile";

    public static final String DESCRIPTION = "View user profile";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[VIEW USER PROFILE]");
        final User user = getAuthService().getUser();
        System.out.println("ID: " + user.getId());
        System.out.println("login: " + user.getLogin());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
