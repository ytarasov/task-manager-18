package ru.t1.ytarasov.tm.command.user;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "change-password";

    public static final String DESCRIPTION = "Change password";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE PASSWORD]");
        final String userId = getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        System.out.println("ENTER NEW PASSWORD");
        final String newPassword = TerminalUtil.nextLine();
        getUserService().setPassword(userId, newPassword);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
