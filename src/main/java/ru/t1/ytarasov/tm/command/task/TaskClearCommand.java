package ru.t1.ytarasov.tm.command.task;

import ru.t1.ytarasov.tm.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";

    public static final String DESCRIPTION = "Clear all tasks";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR TASK LIST]");
        getTaskService().clear();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
