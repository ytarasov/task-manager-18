package ru.t1.ytarasov.tm.command.system;

import ru.t1.ytarasov.tm.exception.AbstractException;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Show information about application developer";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[ABOUT]");
        System.out.println("Yuriy Tarasov");
        System.out.println("ytarasov@t1-consulting.ru");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
