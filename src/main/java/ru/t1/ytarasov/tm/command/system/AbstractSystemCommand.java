package ru.t1.ytarasov.tm.command.system;

import ru.t1.ytarasov.tm.api.service.ICommandService;
import ru.t1.ytarasov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
