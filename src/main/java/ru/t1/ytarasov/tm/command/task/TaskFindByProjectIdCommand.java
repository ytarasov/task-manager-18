package ru.t1.ytarasov.tm.command.task;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskFindByProjectIdCommand extends AbstractTaskCommand {

    public static final String NAME ="task-show-by-project-id";

    public static final String DESCRIPTION = "Show task by project id.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("FIND ALL TASKS BY PROJECT ID");
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks =getTaskService().findAllTasksByProjectId(projectId);
        for (Task task : tasks) System.out.println(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
