package ru.t1.ytarasov.tm.command.task;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    public static final String NAME ="task-bind-to-project";

    public static final String DESCRIPTION = "Bind task to project.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(taskId, projectId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
